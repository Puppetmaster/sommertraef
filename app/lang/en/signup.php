<?php

return array (
	'headline' => 'Registration of Participants',
	'title' => 'Registration',
	'headlineText' => 'Registration for <strong>participants</strong>. Fields marked with <span style="color: red">*</span> are mandatory.',
	'leader' =>
		array (
			'title' => 'Delegation leader',
			'rank' =>
				array (
					'label' => 'Delegation leaders rank:',
					'placeholder' => 'Rank',
				),
			'name' =>
				array (
					'label' => 'Delegation leaders name:',
					'placeholder' => 'Name',
				),
			'unit' =>
				array (
					'label' => 'Unit:',
					'placeholder' => 'Unit',
				),
			'address' =>
				array (
					'label' => 'Address of the unit:',
					'placeholder' => 'Address',
				),
			'phone' =>
				array (
					'label' => '*Delegation leaders phone number:',
					'placeholder' => 'Phone number',
				),
			'email' =>
				array (
					'label' => 'Delegation leader email:',
					'placeholder' => 'Email',
				),
		),
	'attendants-title' => 'Participants',
	'attendants-add' => 'Add participant',
	'attendants' =>
		array (
			'rank' =>
				array (
					'label' => 'Participants rank',
					'placeholder' => 'Rank',
				),
			'name' =>
				array (
					'label' => 'Participants name',
					'placeholder' => 'Name',
				),
			'unit' =>
				array (
					'label' => 'Unit',
					'placeholder' => 'Unit',
				),
			'arrival' =>
				array (
					'label' => 'Arrival',
					'placeholder' => 'Arrival',
				),
			'contest-buddy' =>
				array (
					'label' => 'Participates in Buddy Contest',
					'checkboxYes' => 'Yes',
					'checkboxNo' => 'No',
				),
			'contest-group' =>
				array (
					'label' => 'Participates in Section Contest',
					'checkboxYes' => 'Yes',
					'checkboxNo' => 'No',
				),
		),
	'comments' =>
		array (
			'title' => 'Comments',
			'text' => 'Comments can be written here if any',
		),
	'submit' => 'Submit',
	'new' => 'New signup registered',
	'confirmation' => array(
		'title' => 	'Thanks for the registration',
		'body' => 'In a few minutes you should recieve an email to the specified delegation leader email'
	),
	'payment' => array(
		'body' => 'Payment can be made as follows:',
		'address' => 'Address',
		'contact' => 'When you have paid, please write an email to',
	)
);