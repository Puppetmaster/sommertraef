<?php

return array (
	'headline' => 'Tilmeldingsskema',
	'title' => 'Tilmelding',
	'headlineText' => 'Tilmelding for <strong>deltagere</strong>. Felter markeret med <span style="color: red">*</span> skal udfyldes.',
	'leader' =>
		array (
			'title' => 'Delegationsleder',
			'rank' =>
				array (
					'label' => 'Delegationsleders grad:',
					'placeholder' => 'Grad',
				),
			'name' =>
				array (
					'label' => 'Delegationsleders navn:',
					'placeholder' => 'Navn',
				),
			'unit' =>
				array (
					'label' => 'Enhed:',
					'placeholder' => 'Enhed',
				),
			'address' =>
				array (
					'label' => 'Enhedens adresse:',
					'placeholder' => 'Adresse',
				),
			'phone' =>
				array (
					'label' => 'Delegationsleders telefonnummer:',
					'placeholder' => 'Telefonnummer',
				),
			'email' =>
				array (
					'label' => 'Delegationsleders email:',
					'placeholder' => 'Email',
				),
		),
	'attendants-title' => 'Deltagere',
	'attendants-add' => 'Tilføj flere deltagere',
	'attendants' =>
		array (
			'rank' =>
				array (
					'label' => 'Grad/MA-nr.',
					'placeholder' => 'Grad',
				),
			'name' =>
				array (
					'label' => 'Navn',
					'placeholder' => 'Navn',
				),
			'unit' =>
				array (
					'label' => 'Enhed',
					'placeholder' => 'Enhed',
				),
			'arrival' =>
				array (
					'label' => 'Ankomst',
					'placeholder' => 'Ankomst',
				),
			'contest-buddy' =>
				array (
					'label' => 'Deltager i Buddy Contest',
					'checkboxYes' => 'Ja',
					'checkboxNo' => 'Nej',
				),
			'contest-group' =>
				array (
					'label' => 'Deltager i gruppekonkurrence',
					'checkboxYes' => 'Ja',
					'checkboxNo' => 'Nej',
				),
		),
	'comments' =>
		array (
			'title' => 'Kommentarer',
			'text' => 'Eventuelle kommentarer kan skrives her',
		),
	'submit' => 'Send tilmelding',
	'new' => 'Der er kommet en ny tilmelding',
	'confirmation' => array(
		'title' => 	'Tak for din tilmelding',
		'body' => 'Der er sendt en bekræftelse til den oplyste emailadresse'
	),
	'payment' => array(
		'title' => 'Betalingsinformation',
		'body' => 'Det er muligt at betale på følgende måde:',
		'informationTitle' => 'Fra danske banker:',
		'address' => 'Addresse',
		'contact' => 'Når du har betalt bedes du skrive en email til',
		'type' => 'domestic',
	),
);