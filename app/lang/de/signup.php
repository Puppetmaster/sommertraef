<?php

return array (
	'headline' => 'Teilnehmeranmeldung',
	'title' => 'Teilnehmeranmeldung',
	'headlineText' => 'Am unteren Seitenende können Sie sehen, wie die Einzahlung erfolgt. Mit <span style="color: red">*</span> sind zu beantworten.',
	'leader' =>
		array (
			'title' => 'Delegationsleiter',
			'rank' =>
				array (
					'label' => 'Dienstgrad des Delegationsleiters / Verantwortlichen:',
					'placeholder' => 'Dienstgrad',
				),
			'name' =>
				array (
					'label' => 'Name des Delegationsleiters / Verantwortlichen:',
					'placeholder' => 'Name',
				),
			'unit' =>
				array (
					'label' => 'Name der gemeldeten Einheit/RK:',
					'placeholder' => 'Einheit/RK',
				),
			'address' =>
				array (
					'label' => 'Anschrift der gemeldeten Einheit / RK:',
					'placeholder' => '',
				),
			'phone' =>
				array (
					'label' => 'TelefonNr. des Delegationsleiters / Verantwortlichen:',
					'placeholder' => 'TelefonNr.',
				),
			'email' =>
				array (
					'label' => 'Email des Delegationsleiters / Verantwortlichen :',
					'placeholder' => 'Email',
				),
		),
	'attendants-title' => 'Teilnehmer',
	'attendants-add' => 'Weitere Teilnehmer', // <<<<---- Tilføj flere deltagere
	'attendants' =>
		array (
			'rank' =>
				array (
					'label' => 'Dienstgrad',
					'placeholder' => 'Dienstgrad',
				),
			'name' =>
				array (
					'label' => 'Name',
					'placeholder' => 'Name',
				),
			'unit' =>
				array (
					'label' => 'Einheit/RK',
					'placeholder' => 'Einheit/RK',
				),
			'arrival' =>
				array (
					'label' => 'Ankunft',
					'placeholder' => 'Ankunft',
				),
			'contest-buddy' =>
				array (
					'label' => 'Nimmt am Buddy Contest teil',
					'checkboxYes' => 'Ja',
					'checkboxNo' => 'Nein',
				),
			'contest-group' =>
				array (
					'label' => 'Nimmt am MMW teil',
					'checkboxYes' => 'Ja',
					'checkboxNo' => 'Nein',
				),
		),
	'comments' =>
		array (
			'title' => 'Kommentare',
			'text' => 'Kommentare', // <<<------
		),
	'submit' => 'Senden', // <<-------
	'new' => ' Neue Meldung', // <-------
	'confirmation' => array(
		'title' => 	'Meldung dankend bestätigt', // <<<--------
		'body' => 'Bestätigung an angegebene E-Mail-Adresse gesendet' // <-----------
	),
	'payment' => array(
		'body' => 'Zahlung kann wie folgt geschehen:',
		'address' => 'Anschrift',
		'contact' => 'Bei Zahlung gleichzeitig als Info EMail an',
	),
);