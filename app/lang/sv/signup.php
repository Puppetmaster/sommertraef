<?php

return array (
	'headline' => 'Anmälningsschema',
	'title' => 'Anmälningsschema',
	'headlineText' => 'Anmälan för <strong>deltagare</strong>. Fält markerade med <span style="color: red">*</span> skall ifyllas.',
	'leader' =>
		array (
			'title' => 'Delegationsledar',
			'rank' =>
				array (
					'label' => 'Delegationsledares grad:',
					'placeholder' => 'Grad',
				),
			'name' =>
				array (
					'label' => 'Delegationsledares namn:',
					'placeholder' => 'Namn',
				),
			'unit' =>
				array (
					'label' => 'Enhet:',
					'placeholder' => 'Enhet',
				),
			'address' =>
				array (
					'label' => 'Enhetens adress:',
					'placeholder' => 'Adress',
				),
			'phone' =>
				array (
					'label' => 'Delegationsledares telefonnummer:',
					'placeholder' => 'Telefonnummer',
				),
			'email' =>
				array (
					'label' => 'Delegationsledares email:',
					'placeholder' => 'Email',
				),
		),
	'attendants-title' => 'Deltagere',
	'attendants-add' => 'Tillfoga fler deltagare',
	'attendants' =>
		array (
			'rank' =>
				array (
					'label' => 'Grad.',
					'placeholder' => 'Grad',
				),
			'name' =>
				array (
					'label' => 'Namn',
					'placeholder' => 'Namn',
				),
			'unit' =>
				array (
					'label' => 'Enhet',
					'placeholder' => 'Enhet',
				),
			'arrival' =>
				array (
					'label' => 'Ankomst',
					'placeholder' => 'Ankomst',
				),
			'contest-buddy' =>
				array (
					'label' => 'Deltager i Buddy Contest',
					'checkboxYes' => 'Ja',
					'checkboxNo' => 'Nej',
				),
			'contest-group' =>
				array (
					'label' => 'Deltager i Grupptävling',
					'checkboxYes' => 'Ja',
					'checkboxNo' => 'Nej',
				),
		),
	'comments' =>
		array (
			'title' => 'Kommentarer',
			'text' => 'Eventuella kommentarer kan skrivas här',
		),
	'submit' => 'Schicka anmälan',
	'new' => 'Det har kommit en ny anmälan',
	'confirmation' => array(
		'title' => 	'Tack för din anmälan',
		'body' => 'Bekräftelse har sänts till angiven mailadress'
	),
	'payment' => array(
		'body' => 'Det går att betala på följande sätt:',
		'address' => 'Adress',
		'contact' => 'När du har betalat skicka e-post till',
	),
);