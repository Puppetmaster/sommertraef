<?php

class HelperController extends \BaseController {

	/**
	 * Show the form
	 *
	 * @return Response
	 */
	public function index()
	{
		$language = Input::get('language', 'da');
		$sections = explode('-', $language);
		App::setLocale($sections[0]);

		$inputs = Input::old();
		if (empty($inputs)) {
			$errors = Input::old('errors');
			if (empty($errors)) {
				$errors = ['leader' => new Illuminate\Support\MessageBag()];
			}

			$inputs = Input::only(
				'leader.rank',
				'leader.name',
				'leader.unit',
				'leader.address',
				'leader.phone',
				'leader.email',
				'comment'
			);

			$inputs['errors'] = $errors;
			$inputs['attendants'] = $this->getHelpers();
		}


		$inputs['hidePayment'] = (isset($sections[1]) && $sections[1] == 'hdfyn');
		$inputs['language'] = $language;

		$this->layout->content = \View::make('helper.index', $inputs);
	}

	/**
	 * Send an email or redirct to form
	 *
	 * @return Response
	 */
	public function post() {

		// Do validation
		$emailValidator = Validator::make(
			Input::only(
				'email'
			),
			array(
				'email' => 'required|email'
			)
		);
		if ($emailValidator->fails()) {
			return \Redirect::route('helper')->withInput([
				'errors' => ['leader' => $emailValidator->messages()],
				'email' => Input::get('email'),
				'helpers' => $this->getHelpers(),
				'meals' => $this->getMeals(),
				'comment' => Input::get('comment'),
			]);
		}

		$email = Input::get('email');

		// Send mail to user
		Mail::send(array('emails.helper.html', 'emails.helper.text'), array(
			'email' => $email,
			'helpers' => $this->getHelpers(),
			'comment' => Input::get('comment'),
			'meals' => $this->getMeals()
		), function(Illuminate\Mail\Message $message) use ($email)
		{
			$message
				->to($email)
				->subject(trans('signup.new'));
		});

		// Send mail to koordinater
		Mail::send(array('emails.helper.html', 'emails.helper.text'), array(
			'email' => $email,
			'helpers' => $this->getHelpers(),
			'comment' => Input::get('comment'),
			'meals' => $this->getMeals()
		), function(Illuminate\Mail\Message $message)
		{
			$message
				->to('koordinator@sommertraef.dk', 'Sommertraef Koordinator')
				->subject(trans('signup.new'));
		});

		return \Redirect::route('helper/confirmation');
	}

	public function confirmation() {
		$this->layout->content = \View::make('helper.confirmation');
	}

	protected function toCarbon($time) {
		try {
			return \Carbon\Carbon::createFromFormat('Y/m/d H:i', $time);
		} catch (InvalidArgumentException $e) {
			return null;
		}
	}

	protected function getMeals() {
		$inputs = Input::only(
			'meals'
		);

		$meals = array();
		foreach (['thursday', 'friday', 'saturday', 'sunday'] as $day) {
			foreach (['breakfast', 'lunch', 'dinner'] as $meal) {
				$meals[$day][$meal] = !empty($inputs['meals'][$day][$meal]);
			}
		}

		return $meals;
	}

	protected function getHelpers() {
		$inputs = Input::only(
			'helper'
		);


		if (empty($inputs['helper'])) {
			$inputs['helper'] = array();
		} else {
			foreach ($inputs['helper'] as $index => $attendant) {
				$attendant = Input::only(
					'helper.' . $index . '.rank',
					'helper.' . $index . '.name',
					'helper.' . $index . '.unit',
					'helper.' . $index . '.arrival',
					'helper.' . $index . '.departure',
					'helper.' . $index . '.friday',
					'helper.' . $index . '.saturday'
				);

				if (empty($inputs['helper'][$index]['name'])) {
					unset($inputs['helper'][$index]);
					continue;
				}

				$inputs['helper'][$index] = $attendant['helper'][$index];
			}
		}

		return $inputs['helper'];
	}

}
