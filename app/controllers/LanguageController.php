<?php

class LanguageController extends \BaseController {

	/**
	 * Display the specified resource.
	 *
	 * @param $lang The language code
	 * @param $section The section to show
	 * @return Response
	 */
	public function show($lang, $section)
	{

		$sections = explode('-', $lang);
		App::setLocale($sections[0]);

		$strings = trans($section);
		if (isset($sections[1]) && $sections[1] == 'hdfyn') {
			unset($strings['payment']);
		}

		return Response::json($strings);

	}


}
