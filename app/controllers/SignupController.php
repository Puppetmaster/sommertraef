<?php

class SignupController extends \BaseController {

	/**
	 * Show the form
	 *
	 * @return Response
	 */
	public function index()
	{
		$language = Input::get('language', 'da');
		$sections = explode('-', $language);
		App::setLocale($sections[0]);

		$inputs = Input::old();
		if (empty($inputs)) {
			$errors = Input::old('errors');
			if (empty($errors)) {
				$errors = ['leader' => new Illuminate\Support\MessageBag()];
			}

			$inputs = Input::only(
				'leader.rank',
				'leader.name',
				'leader.unit',
				'leader.address',
				'leader.phone',
				'leader.email',
				'comment'
			);

			$inputs['errors'] = $errors;
			$inputs['attendants'] = $this->getAttendants();
		}


		$inputs['hidePayment'] = (isset($sections[1]) && $sections[1] == 'hdfyn');
		$inputs['language'] = $language;

		$this->layout->content = \View::make('signup.index', $inputs);
	}

	/**
	 * Send an email or redirct to form
	 *
	 * @return Response
	 */
	public function post() {

		$language = Input::get('language', 'da');
		App::setLocale($language);

		// Do validation
		$leaderValidator = Validator::make(
			Input::only(
				'leader.rank',
				'leader.name',
				'leader.unit',
				'leader.address',
				'leader.phone',
				'leader.email'
			)['leader'],
			array(
				'name' => 'required',
				'unit' => 'required',
				'address' => 'required',
				'phone' => 'required',
				'email' => 'required|email'
			)
		);
		if ($leaderValidator->fails()) {
			return \Redirect::route('signup')->withInput([
				'errors' => ['leader' => $leaderValidator->messages()],
				'leader' => Input::get('leader'),
				'attendants' => $this->getAttendants(),
				'comment' => Input::get('comment'),
				'language' => $language,
			]);
		}

		$leader = Input::get('leader');

		// Send mail to user
		Mail::send(array('emails.signup.html', 'emails.signup.text'), array(
			'leader' => $leader,
			'attendants' => $this->getAttendants(),
			'comment' => Input::get('comment'),
		), function(Illuminate\Mail\Message $message) use ($leader)
		{
			$message
				->to($leader['email'], $leader['name'])
				->subject(trans('signup.new'));
		});

		Mail::send(array('emails.signup.html', 'emails.signup.text'), array(
			'leader' => $leader,
			'attendants' => $this->getAttendants(),
			'comment' => Input::get('comment'),
		), function(Illuminate\Mail\Message $message) use ($leader)
		{
			$message
				->to('koordinator@sommertraef.dk', 'Sommertraef Koordinator')
				->subject(trans('signup.new'));
		});

		return \Redirect::route('signup/confirmation', array('language' => $language));
	}

	public function confirmation() {
		$language = Input::get('language', 'da');
		App::setLocale($language);
		$this->layout->content = \View::make('signup.confirmation');
	}

	protected function toCarbon($time) {
		try {
			return \Carbon\Carbon::createFromFormat('Y/m/d H:i', $time);
		} catch (InvalidArgumentException $e) {
			return null;
		}
	}

	protected function getAttendants() {
		$inputs = Input::only(
			'attendant'
		);

		if (empty($inputs['attendant'])) {
			$inputs['attendant'] = array();
		} else {
			foreach ($inputs['attendant'] as $index => $attendant) {
				$attendant = Input::only(
					'attendant.' . $index . '.rank',
					'attendant.' . $index . '.name',
					'attendant.' . $index . '.unit',
					'attendant.' . $index . '.arrival',
					'attendant.' . $index . '.buddy',
					'attendant.' . $index . '.group'
				);

				if (empty($inputs['attendant'][$index]['name'])) {
					unset($inputs['attendant'][$index]);
					continue;
				}

				$inputs['attendant'][$index] = $attendant['attendant'][$index];
				if ($inputs['attendant'][$index]['arrival']) {
					$inputs['attendant'][$index]['arrival'] = $this->toCarbon($inputs['attendant'][$index]['arrival']);
				}
			}
		}

		return $inputs['attendant'];
	}

}
