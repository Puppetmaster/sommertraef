@section('title')
{{ trans('signup.title') }}
@stop

@section('content')

    <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{{ Config::get('app.url') }}}">Sommertraef - <span class="lang-title">Tilmelding</span></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
						<li @if($language == 'da-hdfyn') class="active" @endif>
							<a href="#da-hdfyn" class="select-lang"><img style="height: 16px;" src="http://sommertraef.dk/images/hdfyn.png" alt="HDFYN" /> HD Fyn</a>
						</li>
                        <li @if($language == 'da') class="active" @endif>
                            <a href="#da" class="select-lang"><img src="{{ Config::get('app.url') }}/sprites/blank.gif" class="flag flag-dk" alt="Danish" /> Dansk</a>
                        </li>
                        <li @if($language == 'en') class="active" @endif>
                            <a href="#en" class="select-lang"><img src="{{ Config::get('app.url') }}/sprites/blank.gif" class="flag flag-gb" alt="English" /> English</a>
                        </li>
                        <li @if($language == 'de') class="active" @endif>
                            <a href="#de" class="select-lang"><img src="{{ Config::get('app.url') }}/sprites/blank.gif" class="flag flag-de" alt="Deutsch" /> Deutsch</a>
                        </li>
						<li @if($language == 'sv') class="active" @endif>
							<a href="#sv" class="select-lang"><img src="{{ Config::get('app.url') }}/sprites/blank.gif" class="flag flag-se" alt="Svenska" /> Svenska</a>
						</li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="lang-headline">{{ trans('signup.headline') }}</h1>
                    <p class="lead lang-headline-text">{{ trans('signup.headlineText') }}</p>
                    <form action="{{{ action('SignupController@post') }}}" method="post">
    					<input type="hidden" id="lang" name="language" value="{{{ $language }}}" />

    					<div class="form-horizontal">
    						<div class="form-group">
    							<label for="delegationLeaderRank" class="col-sm-2 control-label lang-leader-rank-label">{{ trans('signup.leader.rank.label') }}</label>
    							<div class="col-sm-10">
    								<input type="text" name="leader[rank]" class="form-control lang-leader-rank-input" id="delegationLeaderRank" placeholder="{{ trans('signup.leader.rank.placeholder') }}" value="{{{ $leader['rank'] }}}">
    							</div>
    						</div>
    						<div class="form-group required @if($errors['leader']->has('name')) has-error @endif">
    							<label for="delegationLeaderName" class="col-sm-2 control-label lang-leader-name-label">{{ trans('signup.leader.name.label') }}</label>
    							<div class="col-sm-10">
    								<input type="text" name="leader[name]" class="form-control lang-leader-name-input" required="required" id="delegationLeaderName" placeholder="{{ trans('signup.leader.name.placeholder') }}" value="{{{ $leader['name'] }}}">
									@if($errors['leader']->has('name')) <span class="help-block">{{{ $errors['leader']->get('name')[0] }}}</span>@endif
    							</div>
    						</div>
    						<div class="form-group required @if($errors['leader']->has('unit')) has-error @endif">
    							<label for="delegationLeaderUnit" class="col-sm-2 control-label lang-leader-unit-label">{{ trans('signup.leader.unit.label') }}</label>
    							<div class="col-sm-10">
    								<input type="text" name="leader[unit]" class="form-control lang-leader-unit-input" required="required" id="delegationLeaderUnit" placeholder="{{ trans('signup.leader.unit.placeholder') }}" value="{{{ $leader['unit'] }}}">
									@if($errors['leader']->has('unit')) <span class="help-block">{{{ $errors['leader']->get('unit')[0] }}}</span>@endif
    							</div>
    						</div>
							<div class="form-group required @if($errors['leader']->has('address')) has-error @endif">
								<label for="delegationLeaderAddress" class="col-sm-2 control-label lang-leader-address-label">{{ trans('signup.leader.address.label') }}</label>
								<div class="col-sm-10">
									<textarea name="leader[address]" class="form-control lang-leader-address-input" required="required" id="delegationLeaderAddress" placeholder="{{ trans('signup.leader.address.placeholder') }}">{{{ $leader['address'] }}}</textarea>
									@if($errors['leader']->has('address')) <span class="help-block">{{{ $errors['leader']->get('address')[0] }}}</span>@endif
								</div>
							</div>
    						<div class="form-group required @if($errors['leader']->has('phone')) has-error @endif">
    							<label for="delegationLeaderPhone" class="col-sm-2 control-label lang-leader-phone-label">{{ trans('signup.leader.phone.label') }}</label>
    							<div class="col-sm-10">
    								<input type="tel" name="leader[phone]" class="form-control lang-leader-phone-input" required="required" id="delegationLeaderPhone" placeholder="{{ trans('signup.leader.phone.placeholder') }}" value="{{{ $leader['phone'] }}}">
									@if($errors['leader']->has('phone')) <span class="help-block">{{{ $errors['leader']->get('phone')[0] }}}</span>@endif
    							</div>
    						</div>
    						<div class="form-group required @if($errors['leader']->has('email')) has-error @endif">
    							<label for="delegationLeaderEmail" class="col-sm-2 control-label lang-leader-email-label">{{ trans('signup.leader.email.label') }}</label>
    							<div class="col-sm-10">
    								<input type="email" name="leader[email]" class="form-control lang-leader-email-input" required="required" id="delegationLeaderEmail" placeholder="{{ trans('signup.leader.email.placeholder') }}" value="{{{ $leader['email'] }}}">
									@if($errors['leader']->has('email')) <span class="help-block">{{{ $errors['leader']->get('email')[0] }}}</span>@endif
    							</div>
    						</div>
    					</div>

    					<fieldset>
    						<div class="control-group">
    	  					  <label class="control-label lang-attendant-title">{{ trans('signup.attendants-title') }}</label>
    	  					  <div class="controls">
    								<table class="table table-striped">
    									<thead>
    										<tr>
    											<th class="lang-attendant-rank-label">{{ trans('signup.attendants.rank.label') }}</th>
    											<th class="lang-attendant-name-label">{{ trans('signup.attendants.name.label') }}</th>
    											<th class="lang-attendant-unit-label">{{ trans('signup.attendants.unit.label') }}</th>
    											<th class="lang-attendant-arrival-label">{{ trans('signup.attendants.arrival.label') }}</th>
    											<th class="lang-attendant-contest-buddy-label">{{ trans('signup.attendants.contest-buddy.label') }}</th>
    											<th class="lang-attendant-contest-group-label">{{ trans('signup.attendants.contest-group.label') }}</th>
    										</tr>
    									</thead>
    									<tbody id="helpers">
@foreach($attendants as $index => $attendant)
                                            <tr>
                                                			<td><input type="text" class="form-control lang-attendant-rank-input" name="attendant[{{{ $index }}}][rank]" placeholder="{{ trans('signup.attendants.rank.placeholder') }}" value="{{{ $attendant['rank'] }}}"></td>
                                                			<td><input type="text" class="form-control lang-attendant-name-input" name="attendant[{{{ $index }}}][name]" placeholder="{{ trans('signup.attendants.name.placeholder') }}" value="{{{ $attendant['name'] }}}"></td>
                                                			<td><input type="text" class="form-control lang-attendant-unit-input" name="attendant[{{{ $index }}}][unit]" placeholder="{{ trans('signup.attendants.unit.placeholder') }}" value="{{{ $attendant['unit'] }}}"></td>
                                                			<td><input type="text" class="form-control lang-attendant-arrival-input" name="attendant[{{{ $index }}}][arrival]" placeholder="{{ trans('signup.attendants.arrival.placeholder') }}" value="{{{ $attendant['arrival'] ? $attendant['arrival']->format('Y-m-d\TH:i') : '' }}}"></td>
															<td>
																<input type="radio" class="lang-attendant-buddy-input" id="helper-{{{ $index }}}-buddy-yes" name="attendant[{{{ $index }}}][buddy]" @if($attendant['buddy']) checked="checked" @endif value="1"><label style="padding-left: 10px; padding-top: 5px;" for="helper-{{{ $index }}}-buddy-yes" class="lang-attendant-contest-buddy-checkbox-yes">{{ trans('signup.attendants.contest-buddy.checkboxYes') }}</label><br />
																<input type="radio" class="lang-attendant-buddy-input" id="helper-{{{ $index }}}-buddy-no" name="attendant[{{{ $index }}}][buddy]" @if(!$attendant['buddy']) checked="checked" @endif value="0"><label style="padding-left: 10px; padding-top: 5px;" for="helper-{{{ $index }}}-buddy-no" class="lang-attendant-contest-buddy-checkbox-no">{{ trans('signup.attendants.contest-buddy.checkboxNo') }}</label>
															</td>
															<td>
																<input type="radio" class="lang-attendant-group-input" id="helper-{{{ $index }}}-group-yes" name="attendant[{{{ $index }}}][group]" @if($attendant['group']) checked="checked" @endif value="1"><label style="padding-left: 10px; padding-top: 5px;" for="helper-{{{ $index }}}-group-yes" class="lang-attendant-contest-group-checkbox-yes">{{ trans('signup.attendants.contest-group.checkboxYes') }}</label><br />
																<input type="radio" class="lang-attendant-group-input" id="helper-{{{ $index }}}-group-no" name="attendant[{{{ $index }}}][group]" @if(!$attendant['group']) checked="checked" @endif value="0"><label style="padding-left: 10px; padding-top: 5px;" for="helper-{{{ $index }}}-group-no" class="lang-attendant-contest-group-checkbox-no">{{ trans('signup.attendants.contest-group.checkboxNo') }}</label>
															</td>
                                                		</tr>
@endforeach
    									</tbody>
    									<tfooter>
    										<tr>
    											<td colspan="8" class="text-center"><button type="button" id="add-helper" class="btn btn-default">
    												<i class="fa fa-plus"></i> <span class="lang-attendant-add">{{ trans('signup.attendants-add') }}</span>
    											</button></td>
    										</tr>
    									</tfooter>
    								</table>
    							</div>
    						</div>

    						<!-- Text input-->
    						<div class="control-group">
    							<label class="control-label lang-comments-title" for="comments">{{ trans('signup.comments.title') }}</label>
    							<div class="controls">
    								<textarea class="form-control" rows="3" name="comment">{{{ $comment  }}}</textarea>
    								<p class="help-block lang-comments-text">{{ trans('signup.comments.text') }}</p>
    							</div>
    						</div>

    						<!-- Text input-->
    						<div class="control-group">
    							<div class="controls">
    								<button type="submit" class="btn btn-default lang-submit">{{ trans('signup.submit') }}</button>
    							</div>
    						</div>

    						<div style="padding: 5px;"><!-- !--></div>
    					</fieldset>
    				</form>
                </div>
            </div>
            <!-- /.row -->
			<div class="row payment-container" @if($hidePayment) style="display: none" @endif>
				<div class="col-lg-12">

					<p class="lead lang-payment-body payment-foreign-hide" @if (!Lang::has('signup.payment.type') || Lang::has('signup.payment.type') != 'domestic') style="display: none" @endif>{{{ trans('signup.payment.body')  }}}</p>


					<div class="foreign-payment-details" @if (!Lang::has('signup.payment.type') || Lang::has('signup.payment.type') != 'domestic') style="display: none" @endif>
						<p>SWIFT/BIC: DABADKKK</p>
						<p>IBAN: DK42 3000 90744222 72</p>
					</div>
					<div class="domestic-payment-details"@if (Lang::has('signup.payment.type') && Lang::has('signup.payment.type') == 'domestic') style="display: none" @endif>
						<p>Danske Bank, Ringe Afd.:</p>
						<p>Reg. Nr. 4692</p>
						<p>Konto Nr. 907 442 2273</p>
						<p>Foreningen til støtte af forsvarsviljen på Midtfyn, Primulavænget 5, 5856 Ryslinge.</p>
					</div>

					<h2 class="lang-payment-address payment-foreign-hide" @if (!Lang::has('signup.payment.type') || Lang::has('signup.payment.type') != 'domestic') style="display: none" @endif>{{{ trans('signup.payment.address')  }}}</h2>
					<address class="payment-foreign-hide" @if (!Lang::has('signup.payment.type') || Lang::has('signup.payment.type') != 'domestic') style="display: none" @endif>
						Foreningen til stoette af forsvarsviljen paa Midtfyn<br />
						Primulavaenget 5,<br />
						DK-5856 Ryslinge
					</address>

					<p><span class="lang-payment-contact">{{{ trans('signup.payment.contact')  }}}</span> <a href="mailto:pay@sommertraef.dk">pay@sommertraef.dk</a></p>

				</div>
			</div>

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="{{ Config::get('app.url') }}/js/jquery-1.11.0.js"></script>
		<script src="{{ Config::get('app.url') }}/js/jquery.datetimepicker.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{ Config::get('app.url') }}/js/bootstrap.min.js"></script>

    	<!-- Custom javascript -->
    	<script src="{{ Config::get('app.url') }}/js/signup.js?test"></script>

    	<table style="display: none;">
    		<tr id="helper-template">
    			<td><input type="text" class="form-control lang-attendant-rank-input" name="attendant[ATTENDANTCOUNT][rank]" placeholder="{{ trans('signup.attendants.rank.placeholder') }}"></td>
    			<td><input type="text" class="form-control lang-attendant-name-input" name="attendant[ATTENDANTCOUNT][name]" placeholder="{{ trans('signup.attendants.name.placeholder') }}"></td>
    			<td><input type="text" class="form-control lang-attendant-unit-input" name="attendant[ATTENDANTCOUNT][unit]" placeholder="{{ trans('signup.attendants.unit.placeholder') }}"></td>
    			<td><input type="text" class="form-control lang-attendant-arrival-input" name="attendant[ATTENDANTCOUNT][arrival]" placeholder="{{ trans('signup.attendants.arrival.placeholder') }}"></td>
    			<td>
					<input type="radio" class="lang-attendant-buddy-input" id="helper-ATTENDANTCOUNT-buddy-yes" name="attendant[ATTENDANTCOUNT][buddy]" value="1"><label style="padding-left: 10px; padding-top: 5px;" for="helper-ATTENDANTCOUNT-buddy-yes" class="lang-attendant-contest-buddy-checkbox-yes">{{ trans('signup.attendants.contest-buddy.checkboxYes') }}</label><br />
					<input type="radio" class="lang-attendant-buddy-input" id="helper-ATTENDANTCOUNT-buddy-no" name="attendant[ATTENDANTCOUNT][buddy]" checked="checked" value="0"><label style="padding-left: 10px; padding-top: 5px;" for="helper-ATTENDANTCOUNT-buddy-no" class="lang-attendant-contest-buddy-checkbox-no">{{ trans('signup.attendants.contest-buddy.checkboxNo') }}</label>
				</td>
    			<td>
					<input type="radio" class="lang-attendant-group-input" id="helper-ATTENDANTCOUNT-group-yes" name="attendant[ATTENDANTCOUNT][group]" value="1"><label style="padding-left: 10px; padding-top: 5px;" for="helper-ATTENDANTCOUNT-group-yes" class="lang-attendant-contest-group-checkbox-yes">{{ trans('signup.attendants.contest-group.checkboxYes') }}</label><br />
					<input type="radio" class="lang-attendant-group-input" id="helper-ATTENDANTCOUNT-group-no" name="attendant[ATTENDANTCOUNT][group]" checked="checked" value="0"><label style="padding-left: 10px; padding-top: 5px;" for="helper-ATTENDANTCOUNT-group-no" class="lang-attendant-contest-group-checkbox-no">{{ trans('signup.attendants.contest-group.checkboxNo') }}</label>
				</td>
    		</tr>
    	</table>

        <script type="text/javascript">
			var URL = "{{{ Config::get('app.url')  }}}";
	        var attendantCount = {{{ count($attendants) }}};
        </script>

@stop