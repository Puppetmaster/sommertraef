@section('title')
Tilmelding
@stop

@section('content')

    <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{{ Config::get('app.url') }}}">Sommertraef - <span class="lang-title">Tilmelding</span></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#da" class="select-lang"><img src="{{ Config::get('app.url') }}/sprites/blank.gif" class="flag flag-dk" alt="Danish" /> Dansk</a>
                        </li>
                        <li>
                            <a href="#en" class="select-lang"><img src="{{ Config::get('app.url') }}/sprites/blank.gif" class="flag flag-gb" alt="English" /> English</a>
                        </li>
                        <li>
                            <a href="#de" class="select-lang"><img src="{{ Config::get('app.url') }}/sprites/blank.gif" class="flag flag-de" alt="Deutsch" /> Deutsch</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="lang--headline">{{{ trans('signup.confirmation.title') }}}</h1>
                    <p><span class="lead lang-headline-text">{{{ trans('signup.confirmation.body') }}}</span>.</p>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="{{ Config::get('app.url') }}/js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{ Config::get('app.url') }}/js/bootstrap.min.js"></script>

		<!-- Custom javascript -->
		<script src="{{ Config::get('app.url') }}/js/confirmation.js"></script>

@stop