@section('title')
	Hjælper
@stop

@section('content')


	<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand lang-title" href="#">Sommertraef</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
				<h1 class="lang-headline">Tilmelding for hj&aelig;lpere</h1>
				<p>Felter markeret med <span style="color: red">*</span> skal udfyldes.</p>
                <form action="{{{ action('HelperController@post') }}}" method="post">
					
					<fieldset>
						<div class="control-group">
	  					  <div class="controls">
								<table class="table table-striped helper-table">
									<thead>
										<tr>
											<th class="lang-helper-rank required">MA-nr/Grad</th>
											<th class="lang-helper-name required">Navn</th>
											<th class="lang-helper-unit required">Enhed</th>
											<th class="lang-helper-email required">Email</th>
											<th class="lang-helper-arrival required">Ankomst</th>
											<th class="lang-helper-departure required">Afrejse</th>
											<th class="lang-helper-assignment-friday">Opgave fredag</th>
											<th class="lang-helper-assignment-saturday">Opgave l&oslash;rdag</th>
										</tr>
									</thead>
									<tbody id="helpers">
									</tbody>
									<tfooter>
										<tr>
											<td colspan="8" class="text-center"><button type="button" id="add-helper" class="btn btn-default">
												<i class="fa fa-plus"></i> <span class="lang-helper-add">Tilf&oslash;j flere hj&aelig;lpere</span>
											</button></td>
										</tr>
									</tfooter>
								</table>
							</div>
						</div>
					
						<!-- Text input-->
						<div class="form-group required">
							<label class="control-label lang-email-title" for="email">Email</label>
							<div class="controls">
								<input class="lang-email-input" id="email" name="email" type="email" required="required" placeholder="Email" class="form-control">
								<p class="help-block lang-email-text">Indtast din Email-adresse, s&aring; f&aring;r du en kopi af tilmeldingen</p>
							</div>
						</div>
						
						<div class="control-group">
	  					  <label class="control-label lang-meals-title">Tilmelder mig flg. m&aring;ltider (s&aelig;t kryds)</label>
	  					  <div class="controls">
								<table class="table table-striped">
									<thead>
										<tr>
											<th></th>
											<th class="text-center lang-meals-breakfast">Morgenmad</th>
											<th class="text-center lang-meals-lunch">Middagsmad</th>
											<th class="text-center lang-meals-dinner">Aftensmad</th>
										</tr>
									</thead>
									<tbody class="text-center">
										<tr>
											<th class="lang-meals-dates-3">TOR</th>
											<td></td>
											<td><input name="meals[thursday][lunch]" type="checkbox" value="yes"></td>
											<td><input name="meals[thursday][dinner]" type="checkbox" value="yes"></td>
										</tr>
										<tr>
											<th class="lang-meals-dates-4">FRE</th>
											<td><input name="meals[friday][breakfast]" type="checkbox" value="yes"></td>
											<td><input name="meals[friday][lunch]" type="checkbox" value="yes"></td>
											<td><input name="meals[friday][dinner]" type="checkbox" value="yes"></td>
										</tr>
										<tr>
											<th class="lang-meals-dates-5">L&Oslash;R</th>
											<td><input name="meals[saturday][breakfast]" type="checkbox" value="yes"></td>
											<td><input name="meals[saturday][dinner]" type="checkbox" value="yes"></td>
											<td><input name="meals[saturday][dinner]" type="checkbox" value="yes"></td>
										</tr>
										<tr>
											<th class="lang-meals-dates-6">S&Oslash;N</th>
											<td><input name="meals[sunday][breakfast]" type="checkbox" value="yes"></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<!-- Text input-->
						<div class="control-group">
							<label class="control-label lang-comments-title" for="comments">Kommentarer</label>
							<div class="controls">
								<textarea name="comment" class="form-control" rows="3"></textarea>
								<p class="help-block lang-comments-text">Eventuelle kommentarer kan skrives her</p>
							</div>
						</div>

						<!-- Text input-->
						<div class="control-group">
							<div class="controls">
								<button type="submit" class="btn btn-default lang-submit">Send tilmelding</button>
							</div>
						</div>
						
						<div style="padding: 5px;"><!-- !--></div>
					</fieldset>
				</form>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.0 -->
	<script src="{{ Config::get('app.url') }}/js/jquery-1.11.0.js"></script>
	<script src="{{ Config::get('app.url') }}/js/jquery.datetimepicker.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ Config::get('app.url') }}/js/bootstrap.min.js"></script>

	<!-- Custom javascript -->
	<script src="{{ Config::get('app.url') }}/js/custom.js"></script>

	<table style="display: none;">
		<tr id="helper-template">
			<td><input type="text" class="form-control lang-helper-input-rank" name="helper[HELPERCOUNT][rank]" placeholder="MA-nr/Grad"></td>
			<td><input type="text" class="form-control lang-helper-input-email" name="helper[HELPERCOUNT][name]" placeholder="Navn"></td>
			<td><input type="text" class="form-control lang-helper-input-unit" name="helper[HELPERCOUNT][unit]" placeholder="Enhed"></td>
			<td><input type="email" class="form-control lang-helper-input-email" name="helper[HELPERCOUNT][email]" placeholder="Email"></td>
			<td><input type="text" class="form-control datetime lang-helper-input-arrival" name="helper[HELPERCOUNT][arrival]" placeholder="Ankomst"></td>
			<td><input type="text" class="form-control datetime lang-helper-input-departure" name="helper[HELPERCOUNT][departure]" placeholder="Afrejse"></td>
			<td><input type="text" class="form-control lang-helper-input-assignment-friday" name="helper[HELPERCOUNT][friday]" placeholder="Opgave fredag"></td>
			<td><input type="text" class="form-control lang-helper-input-assignment-saturday" name="helper[HELPERCOUNT][saturday]" placeholder="Opgave l&oslash;rdag"></td>
		</tr>
	</table>

@stop