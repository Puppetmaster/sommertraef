Hej
Der er kommet en ny hjælper tilmelding

Kontakt email: {{ $email }}

MA-nr/Grad  Navn   Enhed   Ankomst Afrejse Opgave fredag   Opgave lørdag
@foreach($helpers as $helper)
{{ $helper['rank'] }}  {{ $helper['name'] }}  {{ $helper['unit'] }}  {{ $helper['arrival'] }}  {{ $helper['friday'] }}  {{ $helper['saturday'] }}
@endforeach


Måltider

@foreach($meals as $day => $x)
@foreach($x as $meal => $checked)
{{ $day  }} - {{ $meal }}: {{ $checked ? 'yes' : 'no' }}
@endforeach
@endforeach

Kommentar:
{{ $comment }}