<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<h2>Hej</h2>
<p>Der er kommet en ny hjælper tilmelding</p>

<p>Kontakt email: {{{ $email }}}</p>

<h3>Hj&aelig;per</h3>
<table>
    <tr>
        <th>MA-nr/Grad</th>
        <th>Navn</th>
        <th>Enhed</th>
        <th>Ankomst</th>
        <th>Afrejse</th>
        <th>Opgave fredag</th>
        <th>Opgave l&oslash;rdag</th>
    </tr>
@foreach($helpers as $helper)
    <tr>
        <td>{{{ $helper['rank'] }}}</td>
        <td>{{{ $helper['name'] }}}</td>
        <td>{{{ $helper['unit'] }}}</td>
        <td>{{{ $helper['arrival'] }}}</td>
        <td>{{{ $helper['departure'] }}}</td>
        <td>{{{ $helper['friday'] }}}</td>
        <td>{{{ $helper['saturday'] }}}</td>
    </tr>
@endforeach
</table>

<h3>M&aring;ltider</h3>
<table>
    <tr>
        <th></th>
        <th>Morgenmad</th>
        <th>Frokost</th>
        <th>Aftensmad</th>
    </tr>
@foreach($meals as $day => $x)
    <tr>
        <td>{{ $day }}</td>
@foreach($x as $meal => $checked)
        <td>{{ $checked ? 'Ja' : 'Nej' }}</td>
@endforeach
    </tr>
@endforeach
</table>

<h2>Kommentar</h2>
{{{ nl2br(e($comment)) }}}

</body>
</html>
