<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>

{{ trans('email.hello') }}.<br />
<p>{{ trans('signup.new') }}</p>

<h2>{{ trans('signup.leader.title') }}</h2>
<table>
    <tr>
        <th>{{ trans('signup.leader.rank.label') }}</th>
        <td>{{{ $leader['rank'] }}}</td>
    </tr>
    <tr>
        <th>{{ trans('signup.leader.name.label') }}</th>
        <td>{{{ $leader['name'] }}}</td>
    </tr>
    <tr>
        <th>{{ trans('signup.leader.unit.label') }}</th>
        <td>{{{ $leader['unit'] }}}</td>
    </tr>
    <tr>
        <th>{{ trans('signup.leader.address.label') }}</th>
        <td>{{ nl2br(e($leader['address'])) }}</td>
    </tr>
    <tr>
        <th>{{ trans('signup.leader.email.label') }}</th>
        <td>{{{ $leader['email'] }}}</td>
    </tr>
    <tr>
        <th>{{ trans('signup.leader.phone.label') }}</th>
        <td>{{{ isset($leader['phone']) ? $leader['phone'] : null }}}</td>
    </tr>
</table>

<h2>{{ trans('signup.attendants-title') }}</h2>
<table>
    <tr>

        <th>{{ trans('signup.attendants.rank.label') }}</th>
        <th>{{ trans('signup.attendants.name.label') }}</th>
        <th>{{ trans('signup.attendants.unit.label') }}</th>
        <th>{{ trans('signup.attendants.arrival.label') }}</th>
        <th>{{ trans('signup.attendants.contest-buddy.label') }}</th>
        <th>{{ trans('signup.attendants.contest-group.label') }}</th>
    </tr>
@foreach($attendants as $attendant)
    <tr>
        <td>{{{ $attendant['rank'] }}}</td>
        <td>{{{ $attendant['name'] }}}</td>
        <td>{{{ $attendant['unit'] }}}</td>
        <td>{{{ $attendant['arrival'] ? $attendant['arrival']->toDateTimeString() : null }}}</td>
        <td>{{{ $attendant['buddy'] ? trans('email.yes') : trans('email.no') }}}</td>
        <td>{{{ $attendant['group'] ? trans('email.yes') : trans('email.no') }}}</td>
    </tr>
@endforeach
</table>

<h2>{{ trans('signup.comments.title') }}</h2>
{{ nl2br(e($comment)) }}

</body>
</html>
