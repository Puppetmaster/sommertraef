{{ trans('email.hello') }}.
{{ trans('signup.new') }}.

{{ trans('signup.leader.title') }}
{{ trans('signup.leader.rank.label') }}:  {{ $leader['rank'] }}
{{ trans('signup.leader.name.label') }}:  {{ $leader['name'] }}
{{ trans('signup.leader.unit.label') }}:  {{ $leader['unit'] }}
{{ trans('signup.leader.address.label') }}:
{{ $leader['address'] }}
{{ trans('signup.leader.email.label') }}: {{ $leader['email'] }}
{{ trans('signup.leader.phone.label') }}:  {{ $leader['phone'] }}

{{ trans('signup.attendants-title') }}
{{ trans('signup.attendants.rank.label') }}   {{ trans('signup.attendants.name.label') }}   {{ trans('signup.attendants.unit.label') }}   {{ trans('signup.attendants.arrival.label') }}  {{ trans('signup.attendants.contest-buddy.label') }}  {{ trans('signup.attendants.contest-group.label') }}
@foreach($attendants as $attendant)
{{ $attendant['rank'] }}  {{ $attendant['name'] }}  {{ $attendant['unit'] }}  {{ $attendant['arrival'] ? $attendant['arrival']->toDateTimeString() : null }} {{ $attendant['buddy'] ? trans('email.yes') : trans('email.no') }}    {{ $attendant['group'] ? trans('email.yes') : trans('email.no') }}
@endforeach

{{ trans('signup.comments.title') }}
{{ $comment }}