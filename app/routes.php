<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Signup
Route::get('/signup', array('as' => 'signup', 'uses' => 'SignupController@index'));
Route::post('/signup/post', array('as' => 'signup/post', 'uses' => 'SignupController@post'));
Route::get('/signup/confirmation', array('as' => 'signup/confirmation', 'uses' => 'SignupController@confirmation'));

// Helper
Route::get('/helper', array('as' => 'helper', 'uses' => 'HelperController@index'));
Route::post('/helper/post', array('as' => 'helper/post', 'uses' => 'HelperController@post'));
Route::get('/helper/confirmation', array('as' => 'helper/confirmation', 'uses' => 'HelperController@confirmation'));

Route::get('/lang/{lang}/{section}', array('as' => 'lang', 'uses' => 'LanguageController@show'));