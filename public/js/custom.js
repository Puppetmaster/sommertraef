$(function() {
	var helperCount = 0;

	$('#add-helper').click(function(e) {
		var row = $('#helper-template').clone();
		row.prop('id', '');
		row.html(row.html().replace(/HELPERCOUNT/g, helperCount++));
		row.find('.datetime').datetimepicker();
		row.hide();
		$('#helpers').append(row);
		row.fadeIn();
	});
	
	$('#add-helper').trigger('click');
});
