$(function() {

	//$('.lang-attendant-arrival-input').datepicker();

	$('.select-lang').click(function(e) {
		e.preventDefault();
		var lang = $(this).attr('href').substr(1);
		$('#lang').val(lang);
		$('.nav.navbar-nav>li').removeClass('active');
		$(this).parent().addClass('active');
		$.getJSON(URL + '/lang/' + lang + '/signup/', function(data) {
			
			// Title and description
			$('.lang-headline').html(data.headline);
			$('.lang-title').html(data.title);
			document.title = 'Sommertraef - ' + data.title;
			$('.lang-headline-text').html(data.headlineText);

			// Leader
			$.each(data.leader, function(index, value) {
				$('.lang-leader-' + index + '-label').html(value['label']);
				$('.lang-leader-' + index + '-input').attr('placeholder', value['placeholder']);
			});

			// Attendants
			$(".lang-attendant-title").text(data['attendants-title']);
			$(".lang-attendant-add").text(data['attendants-add']);
			$.each(data.attendants, function(index, value) {
				$('.lang-attendant-' + index + '-label').html(value['label']);
				if ("placeholder" in value) {
					$('.lang-attendant-' + index + '-input').attr('placeholder', value['placeholder']);
				}
				if ("checkboxYes" in value) {
					$('.lang-attendant-' + index + '-checkbox-yes').html(value['checkboxYes']);
				}
				if ("checkboxNo" in value) {
					$('.lang-attendant-' + index + '-checkbox-no').html(value['checkboxNo']);
				}
			});

			// Comments
			$.each(data.comments, function(index, value) {
				$('.lang-comments-' + index).html(value);
			});

			// Submit
			$('.lang-submit').html(data.submit);

			// Pay info
			if ((typeof data.payment) == 'undefined') {
				$('.payment-container').slideUp();
			} else {
				if ((typeof data.payment.title) == 'undefined') {
					$('.lang-payment-title').hide();
				} else {
					$('.lang-payment-title').text(data.payment.title).show();
				}

				$('.lang-payment-body').text(data.payment.body);
				if ((typeof data.payment.informationTitle) == 'undefined') {
					$('.lang-payment-information-title').hide().text('');
				} else {
					$('.lang-payment-information-title').text(data.payment.informationTitle).show();
				}
				$('.lang-payment-address').text(data.payment.address);
				$('.lang-payment-contact').text(data.payment.contact);
				$('.payment-container').slideDown();

				if ((typeof data.payment.type != 'undefined') && data.payment.type == 'domestic') {
					$('.domestic-payment-details').show();
					$('.foreign-payment-details').hide();
					$('.payment-foreign-hide').hide();
				} else {
					$('.domestic-payment-details').hide();
					$('.foreign-payment-details').show();
					$('.payment-foreign-hide').show();
				}
			}
		});
		
		return false;
	})
	
	$('#add-helper').click(function(e) {
		var row = $('#helper-template').clone();
		row.prop('id', '');
		row.html(row.html().replace(/ATTENDANTCOUNT/g, attendantCount++));
		row.find('.lang-attendant-arrival-input').datetimepicker();
		row.hide();
		$('#helpers').append(row);

		row.fadeIn();
	});
	
	$('#add-helper').trigger('click');
});