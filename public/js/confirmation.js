$(function() {
	
	$('.select-lang').click(function(e) {
		e.preventDefault();
		var lang = $(this).attr('href').substr(1);
		$('#lang').val(lang);
		$.getJSON('../../lang/' + lang + '/signup/', function(data) {
			
			// Title and description
			$('.lang-headline').html(data.confirmation.title);
			$('.lang-title').html(data.title);
			$('.lang-headline-text').html(data.confirmation.body);
			document.title = 'Sommertraef - ' + data.title;

		});
		
		return false;
	})

});
